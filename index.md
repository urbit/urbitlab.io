---

title: Home
date: 2018-01-01
layout: teams

---

## Welcome to the UA Ruhr Bioinformatics Teams (URBiT) website

This page lists teams of researchers in or related to Bioinformatics at an institution of the University Alliance Ruhr.
