---
group: Computational Chemical Biology
www: www.czodrowskilab.org

pi: <a href="https://ccb.tu-dortmund.de/czodrowski/">Prof. Dr. Paul Czodrowski</a>

image: czodrowski.jpg

research: AI and chemical biology, effects of protonation on the binding of small molecules to proteins.

institution: <a href="https://www.tu-dortmund.de">TU Dortmund University</a>

department: <a href="https://ccb.tu-dortmund.de/en/professorships/cb/">Chemical Biology</a>

other: <a href="https://nfdi4chem.de/">nfdi4chem</a>

---
