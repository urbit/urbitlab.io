---
group:  Bioimage Informatics
www: www.bioinf.rub.de/index.html.en
pi: Prof. Dr. Axel Mosig
image: mosig.jpg

research: bioimage informatics, computational pathology, pattern recognition and explainable machine learning.

institution: <a href="https://www.rub.de">Ruhr University Bochum</a>

department: Faculty for Biology and Biotechnology

other: <a href="http://www.prodi.rub.de/">ProDi</a>
---
