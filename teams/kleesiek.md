---
group: Medical Machine Learning
www: www.ikim.uk-essen.de/research-groups-en/medical-machine-learning-en/
pi: Prof. Dr. Dr. Jens Kleesiek
image: kleesiek.jpg

research: image analysis, self- and unsupervised learning, federated learning, multimodal data analysis.

institution: <a href="https://www.uni-due.de">University of Duisburg-Essen</a>

department: <a href="https://www.ikim.uk-essen.de/">Institute for Artificial Intelligence in Medicine (Institut für KI in der Medizin, IKIM)</a>

---
