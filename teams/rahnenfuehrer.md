---
group: Statistical Methods in Genetics and Chemometrics
www: www.statistik.tu-dortmund.de/biometrics-ickstadt.html

pi: <a href="https://www.statistik.tu-dortmund.de/rahnenfuehrer.html">Prof. Dr. Jörg Rahnenführer</a>

image: rahnenfuehrer.jpg

research: applications of statistics in genetics, bioinformatics and medicine, both regarding method development and data analysis; statistical analysis of gene expression data, statistical analysis of clinical data, survival analysis, cluster analysis

institution: <a href="https://www.tu-dortmund.de">TU Dortmund University</a>

department: <a href="https://www.statistik.tu-dortmund.de/faculty_en.html">Faculty of Statistics</a>

other: <a href="https://www.statistik.tu-dortmund.de/sfb823.html">SFB 823</a>; <a href="https://sfb876.tu-dortmund.de/">SFB 876</a>; <a href="https://www.statistik.tu-dortmund.de/grk2624.html">GRK 2624</a>

---
