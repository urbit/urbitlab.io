---
group: Algorithmic Foundations and Education
www: ls11-www.cs.tu-dortmund.de/fischer/start

pi: <a href="https://ls11-www.cs.tu-dortmund.de/staff/fischer">Prof. Dr. Johannes Fischer</a>

image: fischer.jpg

research: data structures, ML-free text indexing, text compression, combinatorics on strings, parallel algorithms, algorithm engineering.

institution: <a href="https://www.tu-dortmund.de">TU Dortmund University</a>

department: <a href="https://www.cs.tu-dortmund.de">Computer Science</a>

other: <a href="https://sfb876.tu-dortmund.de/">SFB 876</a>

---
