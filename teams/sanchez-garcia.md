---
group: Computational Biochemistry
www: www.uni-due.de/computational-biochemistry/group.php

pi: <a href="https://www.uni-due.de/zmb/members/elsa-sanchez-garcia.php">Prof. Dr. Elsa Sánchez-García</a>

image: sanchez-garcia.jpg

research: theoretical approaches to investigate the structural properties and reactivity of complex chemical and biological systems.

institution: <a href="https://www.uni-due.de">University of Duisburg-Essen</a>

department: <a href="https://www.uni-due.de/biology/">Faculty of Biology</a>

other: <a href="https://www.uni-due.de/zmb/index.php">ZMB</a>

---
