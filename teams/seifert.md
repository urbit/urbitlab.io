---
group: AI for Oncology
www: www.ikim.uk-essen.de/research-groups-en/ai-for-oncology-en/
pi: Prof. Dr. Christin Seifert
image: seifert.jpg

research: artificial intelligence in oncology.

institution: <a href="https://www.uni-due.de">University of Duisburg-Essen</a>

department: <a href="https://www.ikim.uk-essen.de/">Institute for Artificial Intelligence in Medicine (Institut für KI in der Medizin, IKIM)</a>

---
