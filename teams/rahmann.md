---
group: Genome Informatics
www: www.rahmannlab.de

pi: <a href="https://www.rahmannlab.de/people/rahmann.html">Prof. Dr. Sven Rahmann</a>

image: rahmann.jpg

research: algorithmic bioinformatics, string algorithms and data structures, sequence analysis, hashing and applications, machine learning applications in bioinformatics; applications to human disease, cancer, microbiology and biodiversity.

institution: <a href="https://www.uni-due.de">University of Duisburg-Essen</a>

department: <a href="https://www.uk-essen.de/en/humangenetik/startseite/">Institute of Human Genetics</a>, <a href="https://www.uk-essen.de">University Hospital Essen</a>

other: <a href="https://ls11-www.cs.tu-dortmund.de">Computer Science XI</a>, <a href="https://www.tu-dortmund.de">TU Dortmund University</a>; <a href="https://sfb876.tu-dortmund.de/">SFB 876</a>; <a href="https://www.wispermed.org/">GRK 2535 (WisPerMed)</a>

---
