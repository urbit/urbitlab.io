---
group: Clinical Bioinformatics
www: www.ruhr-uni-bochum.de/mpc/forschung/medical_bioinformatics/index.html.en
pi: PD. Dr. Martin Eisenacher
image: eisenacher.jpg

research: computational quantitative proteomics.

institution: <a href="https://www.rub.de">Ruhr University of Bochum</a>

department: <a href="https://www.ruhr-uni-bochum.de/mpc/index.html.en">Medical Proteome Center</a>

other: <a href="https://www.prodi.rub.de/">ProDi</a>
---
