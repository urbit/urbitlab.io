---
group: Mathematical Statistics with Applications in Biometrics
www: www.statistik.tu-dortmund.de/biometrics.html

pi: <a href="https://www.statistik.tu-dortmund.de/biometrics-ickstadt.html">Prof. Dr. Katja Ickstadt</a>

image: ickstadt.jpg

research: spatial and spatio-temporal point process modelling with applications in biology and epidemiology, Gaussian process modelling and analysis with various applications, Bayesian methods and Markov Chain Monte Carlo techniques, regression methods for very large, high-dimensional data, classification and clustering methods for genetic data.

institution: <a href="https://www.tu-dortmund.de">TU Dortmund University</a>

department: <a href="https://www.statistik.tu-dortmund.de/faculty_en.html">Faculty of Statistics</a>

other: <a href="https://www.statistik.tu-dortmund.de/sfb823.html">SFB 823</a>; <a href="https://sfb876.tu-dortmund.de/">SFB 876</a>; <a href="https://www.statistik.tu-dortmund.de/grk2624.html">GRK 2624</a>

---
