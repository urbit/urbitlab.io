---
group: Bioinformatics and Computational Biophysics
www: www.uni-due.de/zmb/bioinformatics-computational-biophysics/group.php

pi: <a href="https://www.uni-due.de/zmb/bioinformatics-computational-biophysics/index.php">Prof. Dr. Daniel Hoffmann</a>

image: hoffmann.jpg

research: mathematical and computational models for biological systems, Bayesian analysis; applications to developmental biology, virology immunology, cancer research, or ecology.

institution: <a href="https://www.uni-due.de">University of Duisburg-Essen</a>

department: <a href="https://www.uni-due.de/biology/">Faculty of Biology</a>

other: <a href="https://www.uni-due.de/zmb/index.php">ZMB</a>

---
