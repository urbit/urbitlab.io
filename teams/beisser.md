---
group: Computational Biodiversity
www: dbeisser.de/research/

pi: <a href="https://dbeisser.de/">Dr. Daniela Beisser</a>

image: beisser.jpg

research: Next-generation sequencing data analysis, (meta-)omics analysis, method development with applications in biodiversity, ecology and microbiology.

institution: <a href="https://www.uni-due.de">University of Duisburg-Essen</a>

department: Faculty of Biology, <a href="https://www.uni-due.de/biodiversitaet/">Chair of Biodiversity</a>

other: <a href="http://sfb-resist.de/">SFB 1439 (RESIST)</a>

---
