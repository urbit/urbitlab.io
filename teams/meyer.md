---
group: Data Science
www: www.ikim.uk-essen.de/research-groups-en/data-science-en
pi: Prof. Dr. Folker Meyer
image: meyer.jpg

research: microbiome analysis, data integration, data exchange standards, data privacy. 

institution: <a href="https://www.uni-due.de">University of Duisburg-Essen</a>

department: <a href="https://www.ikim.uk-essen.de/">Institute for Artificial Intelligence in Medicine (Institut für KI in der Medizin, IKIM)</a>

---
