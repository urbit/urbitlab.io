---
group: Algorithms for Reproducible Bioinformatics
www: koesterlab.github.io
pi: Dr. Johannes Köster
image: koester.png

research: reproducible data analysis, variant calling, spatial transcriptomics.

institution: <a href="https://www.uni-due.de">University of Duisburg-Essen</a>

department: <a href="https://www.uk-essen.de/en/humangenetik/startseite/">Institute of Human Genetics</a>, <a href="https://www.uk-essen.de">University Hospital Essen</a>

other: Medical Oncology, Harvard Medical School, Harvard University, Boston, USA
---
