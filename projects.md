---
title: Projects
layout: basic
date: 2018-01-01
---

## URBiT Projects

UA Ruhr Bioinformatics Teams are involved in many small and large-scale research projects across the UA Ruhr universities.
Here, we aim to provide an overview of the major institutional projects with URBiT involvement.

* [DFG GRK/RTG 2535](https://wispermed.org/): Knowledge and data driven personalised medicine at the point-of-care (2021-)
* [DFG GRK/RTG 2624](https://www.statistik.tu-dortmund.de/grk2624.html): Biostatistical Methods for High-Dimensional Data in Toxicology (2021-)
* [DFG SFB/CRC 1439](http://sfb-resist.de/): Multilevel response to stressor increase and release in stream ecosystems (RESIST; 2021-)
* [DFG nfdi4chem](https://nfdi4chem.de/): National Research Data Infrastructure for Chemistry (2021-)
* [DFG SFB/CRC 876](https://sfb876.tu-dortmund.de/index.html): Providing information by resource-constrained data analysis (2011-2023)
* [SFG SFB/CRC 823](https://www.statistik.tu-dortmund.de/sfb823.html): Statistical modelling of nonlinear dynamic processes ()


## URBiT Institutions

UA Ruhr Bioinformatics Teams are also involved in several interdisciplinary institutes, centers and research buildings of the UA Ruhr universities.

* [DoDSC](http://www.dodsc.tu-dortmund.de): Dortmund Data Science Center, TU Dortmund
* [Research Building ProDi](http://prodi.rub.de/): Center for protein diagnostics, RUB
* [ZMB](https://www.uni-due.de/zmb/): Center of Medical Biotechnology, University of Duisburg-Essen ()


<hr />
To add a project, please contact <a href="https://johanneskoester.bitbucket.io/">Johannes Köster</a> (contact info at the bottom of that page) with the details.
