---
title: New Research Training Group "WisPerMed" (GRK 2535)
date: 2021-03-01
image: /news/wispermed.png
imagewidth: 100
layout: basic
---

The German Research Foundation (DFG) is funding 12 full-time doctoral positions in a new Research Training Group [WisPerMed](https://wispermed.org/research-training-group-wispermed-eng/) ("Knowledge and data based personalization of medicine at the point of care").
These doctoral students will help future doctors to access patient-related information quickly and in a proper context.
